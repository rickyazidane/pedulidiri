<?php
    require_once("../config/Connect.php");
    require_once("../models/Category.php");
    $user = new User();

    switch($_GET["op"]){
        case "insert":
            if(empty($_POST["user_id"])){
                if(is_array($data)==true AND count($data)==0){
                    $user->insert_user($_POST["role_id"],$_POST
                    ["user_fullname"], $_POST["user_nickname"], $_POST
                    ["user_email"], $_POST["user_pass"]);
                }else{
                    $user->update_user($_POST["user_id"],$_POST["role_id"],$_POST
                    ["user_fullname"], $_POST["user_nickname"], $_POST
                    ["user_email"], $_POST["user_pass"]);
                }
            }
        break;

        case "getAllUser":
            $data = $user->get_all_user();
            $data = Array();
            foreach($data as $row) {
                $sub_array = array();
                $sub_array[] = $row ["user_fullname"];
                $sub_array[] = $row ["user_nickname"];
                $sub_array[] = $row ["user_email"];
                $sub_array[] = $row["user_pass"];

                if($row["role_id"] == "1"){
                    $sub_array[] = '<span class="label label-success">Pelapor</span>';
                }elseif($row["role_id"]=="2"){
                    $sub_array[] = '<span class="label label-info">Petugas</span>';
                }else{
                    $sub_array[] = '<span class="label label-primary">Administrator</span>';
                }
                
                $sub_array[] = '<button type="button" onClick="edit('.$row["user_id"].'); id="'.
                $row["user_id"].'"
                class="btn btn-info" tittle="Edit User">
                    <div>
                        <i class="fa fa-edit"></i>
                    </div>
                </button>';
                $sub_array[] = '<button type="button" onClick="delete('.$row["user_id"].'); id="'.
                $row["user_id"].'"
                class="btn btn-info" tittle="Delete User">
                    <div>
                        <i class="fa fa-trash"></i>
                    </div>
                </button>';
                $data[] = $sub_array;
                $result = array(
                    "sEcho" => 1,
                    "iTotalRecords" => count($data),
                    "iTotalDisplayRecords" => count($data),
                    "aaData" => $data
                );

                echo json_encode($result);
            }
                
        break;

        case "delete":
            $user->delete_user($_POST["user_id"]);
        break;
    }