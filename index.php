<?php
    require_once("config/Connect.php");

    if(isset($_POST["submit"]) and $_POST["submit"]=="Yes"){
        require_once("models/User.php");
        $user = new User();
        $user->login();
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Hedeh</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="public/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="public/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="public/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="public/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="index2.html"><b>Turu</b>Dulu</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    
<!-- petugas role id 1 dan 2 -->
    <form action="" method="post" id ="login_form">
      <input type="hidden" id="role_id" name="role_id" value="1">
    <p class="login-box-msg" id="lblHeader">Akses Pelapor</p>

<?php
  if(isset($_GET["m"])){
    switch($_GET["m"]){
      case "1";
      ?>
      <div class="callout callout-danger">
        <p>yg salah pasword apa email hayooo</p>
      </div>
    <?php
      break;
      case "2";
      ?>
      <div class="callout callout-info">
        <p>gada isinya cok</p>
      </div>
      <?php
       break;
    }
  }
  ?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" id="user_email" name="user_email" placeholder="E-mail Address">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="user_pass" name="user_pass" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
            <input type="hidden" name="submit" value="Yes">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>

    </form>

    <!-- /.social-auth-links -->
    <br>
    <a id="btnAkses" href="#">Akses Petugas</a>
</br>
    <a id="btnReset" class="float" href="#"> Reset Password</a>
    <br>
    <a href="view/LoginAdmin"> rahasia</a>



  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="public/plugins/iCheck/icheck.min.js"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
<script type="text/javascript" src="index.js"></script>
</body>
</html>
