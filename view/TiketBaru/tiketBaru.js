function init(){
    $("#ticket-form").on("submit", function(e){
        saveAndEdit(e);
    })
  }
  
  $(document).ready(function () {
  
    $.post("../../controller/Category.php?op=combo", function (data, status) {
        $('#cat_select').html(data);
    })
  
  });
  
  function saveAndEdit(e) {
    e.preventDefault();
    var formData = new FormData($("#ticket-form")[0]);
  
    if ($('#textarea').val() == '' || $('tick_name').val() == '') {
      swal("perhatian", "Silahkan isi data terlebih dahulu", "warning");
    } else {
      $.ajax({
        url: "../../controller/Ticket.php?op=insert",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
          $('#tick_name').val('');
          $('#textarea').val('');
          swal("Selamat", "Tiket baru berhasil", "success");
        }
      })
    }
  }
  
  init();