-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2022 at 05:51 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ukk_helpdesk`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`cat_id`, `cat_name`, `status`) VALUES
(1, 'Hardware', 1),
(2, 'Software', 1),
(3, 'Insident', 1),
(4, 'Request Service', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ticket`
--

CREATE TABLE `tb_ticket` (
  `tick_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `tick_name` varchar(100) NOT NULL,
  `tick_descrip` text NOT NULL,
  `tick_status` varchar(15) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_ticket`
--

INSERT INTO `tb_ticket` (`tick_id`, `user_id`, `cat_id`, `tick_name`, `tick_descrip`, `tick_status`, `created_at`, `status`) VALUES
(36, 1, 1, 'adssa', 'dada', 'Opened', '2022-04-14', 1),
(37, 1, 1, 'aaa', 'aaa', 'Opened', '2022-04-14', 1),
(38, 1, 1, 'ada', 'asda', 'Opened', '2022-04-23', 1),
(39, 1, 1, 'ada', 'ada', 'Opened', '2022-04-27', 1),
(40, 1, 3, 'namabaru', 'yeyyy', 'Opened', '2022-04-17', 1),
(41, 1, 1, 'mobil mobilan', 'darimana duidnya', 'Closed', '2022-04-23', 1),
(46, 1, 1, 'yuyu', 'yuyu', 'Opened', '2022-04-19', 1),
(47, 1, 1, 'adsa', 'dadasda', 'Opened', '2022-04-19', 1),
(48, 1, 3, 'asda', 'dadas', 'Opened', '2022-04-19', 1),
(49, 1, 3, 'adad', 'dadas', 'Opened', '2022-04-19', 1),
(50, 1, 2, 'dadas', 'dadasczx', 'Opened', '2022-04-19', 1),
(51, 1, 4, 'dadas', 'dasdas', 'Opened', '2022-04-19', 1),
(52, 1, 3, 'asdsa', 'dasda', 'Opened', '2022-04-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_fullname` varchar(100) DEFAULT NULL,
  `user_nickname` varchar(50) DEFAULT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_pass` varchar(25) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`user_id`, `role_id`, `user_fullname`, `user_nickname`, `user_email`, `user_pass`, `created_at`, `updated_at`, `deleted_at`, `status`) VALUES
(1, 1, 'rickyazidane', 'megumin', 'ricky.azidane@student.smkti.net', 'bakemonogatari', '2022-04-05 03:46:50', '2022-04-05 03:46:50', '2022-04-05 03:46:50', 1),
(4, 2, 'Rikii', 'Rik', 'Riki@gmail.com', '111111', '2022-04-14 06:34:43', '2022-04-14 06:34:43', '2022-04-14 06:34:43', 1),
(6, 3, 'qwerty', 'qwerty', 'qwerty@gmail.com', 'qwerty', '2022-04-20 03:27:40', '2022-04-20 03:27:40', '2022-04-20 03:27:40', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tb_ticket`
--
ALTER TABLE `tb_ticket`
  ADD PRIMARY KEY (`tick_id`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_ticket`
--
ALTER TABLE `tb_ticket`
  MODIFY `tick_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
